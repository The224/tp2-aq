package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class BufferTest {
    //@Mock
    Buffer buffer;
    final int capacity = 10;
/*
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();*/

    @After
    public void tearDown () {
        buffer = null;
    }

    @Test
    public void testCapacity() {

        buffer = new Buffer(capacity);

        assertTrue("Wrong Capacity",buffer.capacity() == 10);
    }

    @Test
    public void testToString() {
        buffer = new Buffer(capacity);

        assertTrue("Return is null",buffer.toString() != null);
    }

    @Test
    public void testGetCurrentLoad() throws BufferFullException {
        buffer = new Buffer(capacity);

        assertTrue("Load 1 is wrong",buffer.getCurrentLoad() == 0);

        buffer.addElement(new Element(12));
        assertTrue("Load 2 is wrong",buffer.getCurrentLoad() == 1);

        buffer.addElement(new Element(2));
        assertTrue("Load 3 is wrong",buffer.getCurrentLoad() == 2);
    }

    @Test(expected = BufferFullException.class)
    public void testAddElementFull() throws BufferFullException {
        final int capacity = 2;
        buffer = new Buffer(capacity);

        buffer.addElement(new Element(1));
        buffer.addElement(new Element(12));
        buffer.addElement(new Element(2));
    }

    @Test(expected = BufferFullException.class)
    public void testAddElementFull2() throws BufferFullException {
        final int capacity = 1;
        buffer = new Buffer(capacity);

        buffer.addElement(new Element(1));
        buffer.addElement(new Element(12));
    }

    @Test(expected = BufferFullException.class)
    public void testAddElementFull3() throws BufferFullException {
        final int capacity = 0;
        buffer = new Buffer(capacity);

        buffer.addElement(new Element(1));
    }

    @Test
    public void testIsFull() throws BufferFullException {
        final int capacity = 3;
        buffer = new Buffer(capacity);

        buffer.addElement(new Element(1));
        buffer.addElement(new Element(1));

        assertFalse("Buffer is not full",buffer.isFull());

        buffer.addElement(new Element(1));

        assertTrue("Buffer is full",buffer.isFull());
    }

    @Test
    public void testIsEmpty() throws BufferFullException {
        final int capacity = 3;
        buffer = new Buffer(capacity);

        assertTrue("Buffer is empty",buffer.isEmpty());

        buffer.addElement(new Element(1));

        assertFalse("Buffer is not empty",buffer.isEmpty());

    }

    @Test(expected = BufferEmptyException.class)
    public void testRemoveElement() throws BufferEmptyException {
        buffer = new Buffer(capacity);

        buffer.removeElement();
    }

    @Test
    public void testRemoveElement2() throws BufferFullException, BufferEmptyException {
        buffer = new Buffer(capacity);

        Element e1 = new Element(1);
        Element e2 = new Element(1);
        Element e3 = new Element(1);
        Element e4 = new Element(1);
        Element e5 = new Element(1);
        Element e6 = new Element(1);

        buffer.addElement(e1);
        assertEquals("Not the same element", e1, buffer.removeElement());

        buffer.addElement(e2);
        buffer.addElement(e3);
        assertEquals("Not the same element 2", e3, buffer.removeElement());

        buffer.addElement(e4);
        buffer.addElement(e5);
        buffer.addElement(e6);

        assertEquals("Not the same element 3", e6, buffer.removeElement());
        assertEquals("Not the same element 4", e5, buffer.removeElement());
        assertEquals("Not the same element 5", e4, buffer.removeElement());
    }






}


