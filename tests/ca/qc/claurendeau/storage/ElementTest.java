package ca.qc.claurendeau.storage;

import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ElementTest {

    Element element;

    @After
    public void tearDown () {
        element = null;
    }

    @Test
    public void testGetData() {
        element = new Element();
        element.setData(8);
        assertTrue("Wrong Data",element.getData() == 8);
    }

    @Test
    public void testGetNext() {
        element = new Element();
        element.setNext(new Element());

        assertTrue("Wrong Next",element.getNext() != null);
    }



}
