package ca.qc.claurendeau.storage;

public class Element
{
    private int data;
    private Element next;

    public Element() {}

    public Element(int data) {
        this.data = data;
    }

    public void setData(int data)
    {
        this.data = data;
    }
    
    public int getData()
    {
        return data;
    }

    public Element getNext()
    {
        return next;
    }

    public void setNext(Element element)
    {
        this.next = element;
    }

    @Override
    public String toString() {
        return "["+data+"],"+next;
    }
}
