package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
    private int capacity;
    private Element head = null;

    public Buffer(int capacity)
    {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Buffer{" +
                "capacity=" + capacity +
                ", stack=" + head +
                '}';
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        int size = 0;
        Element iterator = head;
        if (iterator != null) {
            size++;
            while (iterator.getNext() != null) {
                size++;
                iterator = iterator.getNext();
            }
        }
        return size;
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        if (head != null)
            return false;
        return true;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        if (capacity == getCurrentLoad())
            return true;
        return false;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
        if (isFull())
            throw new BufferFullException();
        Element iterator = head;
        if (iterator != null) {
            while (iterator.getNext() != null) {
                iterator = iterator.getNext();
            }
            iterator.setNext(element);
        } else {
            this.head = element;
        }
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException
    {
        if (head == null)
            throw new BufferEmptyException();

        if (head.getNext() == null) {
            Element element = head;
            head = null;
            return element;
        }

        Element iterator = head;
        while (iterator.getNext() != null && iterator.getNext().getNext() != null) {
            iterator = iterator.getNext();
        }

        Element element = iterator.getNext();
        iterator.setNext(null);

        return element;
    }

}
