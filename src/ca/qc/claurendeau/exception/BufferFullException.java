package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
    public BufferFullException() {
        super("La capacity du buffer a ete depacer.");
    }
}
