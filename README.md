# TP-2 

Remise : mardi 7 novembre à 23h55 EST. 10% en moins par jour de retard.
Note : Ce TP est à faire SEUL.



## INTRODUCTION 

L'entreprise pour laquelle vous travaillez doit développer une queue logicielle (buffer) qui sera utilisée par un système de producteurs/consommateurs/observateur.

Vous devez ‘forker’ et ‘cloner’ un dépôt Git sur bitbucket. Ce dépôt contient un projet éclipse se composant d'un jar (implémentation des classes producteur/consommateur/observateur ainsi que le système de votre client) et de fichiers squelettes pour l'implémentation de la queue logicielle.  

Vous devez m’envoyer une invitation que je puisse lire votre dépôt. (flacoursiere)

La queue logicielle doit stocker des entiers et doit utiliser une liste chainée que vous implémenterez en utilisant la classe Element (squelette donné avec le dépôt).

Votre client est très exigeant. Il veut que vous travailliez avec un système d'intégration continue et que vous suiviez la méthodologie Test Driven Developpement. Vous serez payé non seulement en fonction de la qualité de votre code et de vos tests (incluant le taux de couverture du code et du domaine) mais aussi en fonction du fait que vous aurez mis en place un système d'intégration continue (Jenkins) et que vous aurez suivi la méthodologie TDD à la lettre.



## VOTRE MISSION 

Le client vous demande de faire les choses suivantes :

1. Installez sur votre poste le système d'intégration continue Jenkins. Vous aurez besoin d'installer :
	- maven 3
	- java 1.8
	- jenkins

Vous pouvez installer Virtualbox si vous ne voulez pas installer toutes ces applications sur votre poste.  Il est recommandé d’utiliser alors ‘linux’ si vous procédez de cette façon

2. Configurez Jenkins et liez le à votre dépôt Bitbucket.

3. Ajoutez une job jenkins sous forme de ‘pipeline’ pour faire tourner les tests à chaque changement. Vous pouvez utiliser le polling en le configurant à 1 minutes.

4. Transformez votre projet en projet maven

5. Faites un commit et un push, votre premier build devrait être un succès à cause du seul et unique test que vous avez et qui devrait passer. Votre client l'a mis exprès pour que vous puissiez tester votre système de build.

6. Implémentez la queue (classes Buffer, Element, Exception*) en suivant la méthodologie TDD:

	- a. Écrivez un test qui ne passera pas
	- b. Faites un commit, puis un push
	- c. Observez le build échouer, uniquement à cause de votre test
	- d. Écrivez le code nécessaire pour que le test passe
	- e. Faites un commit, puis un push
	- f. Observez le build passer, grâce au code que vous avez ajouté
	- h. Recommencez jusqu'à ce que vous ayez tout implémenté



### REMISE 

Assurez vous d’avoir le fichier ‘Jenkinsfile’ dans le ‘root’ de votre projet. Votre client a accès à votre dépôt Git et l'utilisera pour évaluer votre travail.



### ÉVALUATION 

Votre client calculera votre prestation en fonction des éléments suivants :

- Installation et utilisation de Jenkins : 20%
- Couverture des tests
	- Code : 15% * taux de couverture du code
	- Domaine : 15% * taux de couverture du domaine
- Implémentation et fonctionnement du produit final : 20%
- Suivi de la méthodologie TDD telle que décrite dans ce document : 30%

Note : Sachez que votre client a lui aussi un système Jenkins qui est lié à votre dépôt Git. Chaque commit que vous pousserez déclenchera un 'maven clean test' sur son CI. Il pourra ainsi suivre l'évolution de votre travail au fil des jours s'il en a envie.



## SYSTÈME DONNÉ 

### Questions

Veuillez écrire à Francois Lacoursiere via Mio.

### Invocation 

ProducerConsumer <producers> <consumers> <buffer size>

-- Exemple de configuration éclipse (run configuration java arguments) --

4 2 2

-- Exemple de sortie finale --

- producer-1: Produced 59
- consumer-1: Consumed 59
- consumer-0: Could not get anything
- ca.qc.claurendeau.exception.BufferEmptyException: Buffer is empty
- producer-3: Produced 48
- consumer-0: Consumed 48
- observer: buffer is empty
- observer: buffer load: 0 / 2
- observer: buffer [  ] 
- producer-0: Produced 25
- producer-0: Produced 2
- consumer-1: Consumed 25
- observer: buffer load: 1 / 2
- observer: buffer [ 2 ] 
- producer-0: Produced 58
- producer-2: Could not store elem 57
- ca.qc.claurendeau.exception.BufferFullException: Buffer is full
- producer-1: Could not store elem 27
- ca.qc.claurendeau.exception.BufferFullException: Buffer is full
- producer-2: Could not store elem 61

[ ... log réduit ... ]

- observer: buffer load: 2 / 2
- observer: buffer [ 53 12 ] 
- producer-3: Could not store elem 30
- ca.qc.claurendeau.exception.BufferFullException: Buffer is full
- producer-3 has excedeed his lifetime (this is normal and OK).
- observer: buffer is full
- observer: buffer load: 2 / 2
- observer: buffer [ 53 12 ] 
- observer: buffer is full
- observer: buffer load: 2 / 2
- observer: buffer [ 53 12 ] 
- observer has excedeed his lifetime (this is normal and OK).


